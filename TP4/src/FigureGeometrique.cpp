#ifndef FIGUREGEOMETRIQUE_CPP
#define FIGUREGEOMETRIQUE_CPP
#include "FigureGeometrique.hpp"
	FigureGeometrique::FigureGeometrique(const Couleur & couleur) :
		_couleur(couleur)
	{}
	const Couleur & FigureGeometrique::getCouleur() const {
		return _couleur;
	}
	void FigureGeometrique::afficher(const Cairo::RefPtr<Cairo::Context>& context) const{}
#endif
