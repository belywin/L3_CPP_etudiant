#include <CppUTest/CommandLineTestRunner.h>
#include "Client.hpp"

TEST_GROUP(GroupClient) { };

TEST(GroupClient, Client_test1)  
{
	Client pCli(42,"toto");
	CHECK_EQUAL( pCli.getId() , 42);
	CHECK_EQUAL( pCli.getNom() , "toto");
}
