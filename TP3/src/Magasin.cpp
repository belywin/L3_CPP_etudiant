#include <iostream>
#include <string>
#include <vector>
#include "Produit.hpp"
#include "Client.hpp"
#include "Location.hpp"
#include "Magasin.hpp"
Magasin::Magasin(){
	_idCourantClient = 0;
	_idCourantProduit = 0;
}
int Magasin::nbClients() const{
	return _client.size();
}
void Magasin::ajouterClient(const std::string & nom){
	_client.push_back(Client(_idCourantClient,nom));
	_idCourantClient ++;
}
void Magasin::afficherClients() const{
	int i;
	std::cout << "Clients du magasin :" << std::endl;
	for(i=0; i < _client.size(); i++){
		std::cout << "	-> " ;
		_client.at(i).afficherClient();
	}
}
void Magasin::supprimerClient(int idClient){
	int i;
	int indice;
	try{
	for(i=0; i < _client.size(); i++){
		if (_client.at(idClient).getId() == idClient){
			std::swap(_client.at(i),_client.back());
			_client.pop_back();
		}
	}
	}
	catch(const std::exception & e){
		std::cout << "Erreur : ce client n'existe pas \n" << std::endl;
	}
}
int Magasin::nbProduits() const{
	return _produit.size();
}
void Magasin::ajouterProduit(const std::string & nom){
	_produit.push_back(Produit(_idCourantProduit,nom));
	_idCourantProduit ++;
}
void Magasin::afficherProduits() const{
	int i;
	std::cout << "Produit du magasin :" << std::endl;
	for(i=0; i < _produit.size(); i++){
		std::cout << "	-> " ;
		_produit.at(i).afficherProduit();
	}
}
void Magasin::supprimerProduit(int idProduit){
	int i;
	for(i=0; i < _produit.size(); i++){
		if (_produit.at(i).getId() == idProduit){
			std::swap(_produit.at(idProduit),_produit.back());
			_produit.pop_back();
		}
		else{
			throw "Erreur : ce client n'existe pas \n";
			throw "Erreur : ce client n'existe pas \n";
		}
	}

}
