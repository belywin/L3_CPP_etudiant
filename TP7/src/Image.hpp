#ifndef IMAGE_HPP
#define IMAGE_HPP
#include <iostream>
class Image{
	private:
	int _largeur;
	int _hauteur;
	int* _pixels;
	public:
	Image(int largeur, int hauteur);
	int getLargeur() const;
	int getHauteur() const;
	/*int getPixel(int i, int j) const;
	void setPixel(int i, int j, int couleur);*/
	const int& pixel(int i, int j) const;
	int & pixel(int i, int j);
	void ecrirePnm(const Image & img, const std::string & nomFichier);
	~Image();
	};
#endif
