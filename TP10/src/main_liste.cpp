#include <iostream>

#include "Liste.hpp"
#include "ListeGenerique.hpp"
#include <string>
int main() {

    Liste l1;
    l1.push_front(37);
    l1.push_front(13);
    std::cout << l1 << std::endl;
	ListeGenerique<float> l2;
    l2.push_front(37.5);
    l2.push_front(13.2);
    std::cout << l2 << std::endl;
    ListeGenerique<std::string> l3;
    l3.push_front("p");
    l3.push_front("q");
    std::cout << l3 << std::endl;
    ListeGenerique<Personne> l4;
    Personne p1;
    p1._nom = "test";
    p1._age = 18; 
    Personne p2;
    p2._nom = "test1";
    p2._age = 19;
    l4.push_front(p1);
    l4.push_front(p2);
    std::cout << l4 << std::endl;
    return 0;
}

