
#ifndef LISTE_GENERIQUE_HPP_
#define LISTE_GENERIQUE_HPP_

#include <cassert>
#include <ostream>
#include <iostream>
#include "Personne.hpp"
template <typename T>
// liste d'entiers avec itérateur
class ListeGenerique {
    
    private:
	struct NoeudGenerique {
			T _valeur;
			NoeudGenerique * _ptrNoeudSuivant;
        };
      
	protected:
    NoeudGenerique * _ptrTete;
    public:
        class iteratorGenerique {
			protected:
			NoeudGenerique * _ptrNoeudCourant;
            public:
            iteratorGenerique(NoeudGenerique * ptrNoeudCourant)
            {
				_ptrNoeudCourant = ptrNoeudCourant;
			}
                const iteratorGenerique & operator++() {
					_ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                T& operator*() const {
                    return _ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iteratorGenerique & iter) const {
					if(iter._ptrNoeudCourant == _ptrNoeudCourant){
						return false;
					}	
                    else{
						return true;
					}
                }

                friend ListeGenerique; 
        };
	
    public:
		ListeGenerique()
		{
			_ptrTete = nullptr;
		}
		~ListeGenerique()
		{
			clear();
		}
        void push_front(T valeur) {
			NoeudGenerique* noeud = new NoeudGenerique;
			noeud->_valeur = valeur;
			noeud->_ptrNoeudSuivant = _ptrTete;
			_ptrTete = noeud;			
        }

        T& front() const {
            return _ptrTete->_valeur;
        }

        void clear() {
			NoeudGenerique* noeud;
			while(_ptrTete != nullptr){
				noeud = _ptrTete->_ptrNoeudSuivant;
				delete _ptrTete;
				_ptrTete = noeud;
			}
        }

        bool empty() const {
			if(_ptrTete != nullptr){
				return false;
			}
			else{
				return true;
			}	
        }

        iteratorGenerique begin() const {
            return iteratorGenerique(_ptrTete);
        }

        iteratorGenerique end() const {
            return iteratorGenerique(nullptr);
        }

};
template <typename T>
std::ostream& operator<<(std::ostream& os, const ListeGenerique<T> & l) {
	/*ListeGenerique<T>::iteratorGenerique iter = l.begin();
	while(iter != l.end()){
		os << *iter << " ";
		++iter;
	}*/
	os << "Liste Générique : " << std::endl;
	for (T& v : l) {
		os << v << " ";
	}
	
    return os;
}

#endif
