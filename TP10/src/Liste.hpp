
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
    
    private:
	struct Noeud {
			int _valeur;
			Noeud * _ptrNoeudSuivant;
        };
	protected:
    Noeud * _ptrTete;
    public:
        class iterator {
			protected:
			Noeud * _ptrNoeudCourant;
            public:
            iterator(Noeud * ptrNoeudCourant)
            {
				_ptrNoeudCourant = ptrNoeudCourant;
			}
                const iterator & operator++() {
					_ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                int& operator*() const {
                    return _ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator & iter) const {
					if(iter._ptrNoeudCourant == _ptrNoeudCourant){
						return false;
					}	
                    else{
						return true;
					}
                }

                friend Liste; 
        };
	
    public:
		Liste()
		{
			_ptrTete = nullptr;
		}
		~Liste()
		{
			clear();
		}
        void push_front(int valeur) {
			Noeud* noeud = new Noeud;
			noeud->_valeur = valeur;
			noeud->_ptrNoeudSuivant = _ptrTete;
			_ptrTete = noeud;			
        }

        int& front() const {
            return _ptrTete->_valeur;
        }

        void clear() {
			Noeud* noeud;
			while(_ptrTete != nullptr){
				noeud = _ptrTete->_ptrNoeudSuivant;
				delete _ptrTete;
				_ptrTete = noeud;
			}
        }

        bool empty() const {
			if(_ptrTete != nullptr){
				return false;
			}
			else{
				return true;
			}	
        }

        iterator begin() const {
            return iterator(_ptrTete);
        }

        iterator end() const {
            return iterator(nullptr);
        }

};

std::ostream& operator<<(std::ostream& os, const Liste& l) {
	Liste::iterator iter = l.begin();
	while(iter != l.end()){
		os << *iter << " ";
		++iter;
	}
    return os;
}

#endif

