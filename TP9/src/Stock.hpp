#ifndef STOCK_HPP
#define STOCK_HPP
#include "Inventaire.hpp"
class Stock{
	private:
	std::map<std::string,float> _produits;
	public:
	void recalculerStock(const Inventaire & inventaire);
}
std::ostream & operator<<(std:ostream & os, Stock & sto);
#endif
