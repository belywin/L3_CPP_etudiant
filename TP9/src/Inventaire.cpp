#include "Inventaire.hpp"
#include <locale>
#include <algorithm>
std::istream& operator >>(std::istream& is, Inventaire& inv) {
	std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    Bouteille b; 
    while(is >> b){
		inv._bouteilles.push_back(b);
	}
	std::locale::global(vieuxLoc);
	return is;
}
std::ostream& operator <<(std::ostream& os, Inventaire& inv) {
	/*std::list<Bouteille>::iterator it;
	for(it = inv._bouteilles.begin(); it != inv._bouteilles.end(); ++it){
		os << it->_nom << ";" << it->_date << ";" << it->_volume << "\n";
	}*/
	for(int i = 0;i < inv._bouteilles.size(); i++){
		os << inv._bouteilles[i];
	}
	return os;
}


void Inventaire::trier(){
	std::sort(_bouteilles.begin(),_bouteilles.end(),
	[](const Bouteille & a, const Bouteille & b)->bool
{ 
    return a._nom < b._nom; 
});
}
