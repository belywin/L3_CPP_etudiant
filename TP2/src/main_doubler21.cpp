#include "Doubler.hpp"
#include "Liste.hpp"
#include <iostream>

int main() {
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;
    int a;
    a = 42;
    int *p(0);
    p = &a;
    *p = 37;
    int *t;
    t = new int[10];
    t[3] = 42;
    std::cout << t[3] << std::endl;
    delete [] t;
    std::cout << t[3] << std::endl;
    t = nullptr;
    
    Liste *liste = new Liste();
    liste->ajouterDevant(5);
    afficherListe(liste);
    liste->ajouterDevant(7);
    afficherListe(liste);
    std::cout << liste->getTaille() << std::endl;
    std::cout << liste->getElement(1) << std::endl;
    std::cout << liste->getElement(2) << std::endl;
    delete liste;
    return 0;
}

